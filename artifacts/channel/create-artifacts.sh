# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF_DecertNetwork/fabric-samples/bin" ] ; then
PATH="/workspaces/HLF_DecertNetwork/fabric-samples/binn:$PATH"
fi
#Generate Crypto artifacts for organizations


# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD


# chmod -R 0755 ./crypto-config

# Delete existing artifacts
rm -rf ./crypto-config
rm nationalschool-genesis.block decert-channel.tx

# rm -rf ../../channel-artifacts/*

cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Generate the genesis block for the National High School Consortium Orderer
configtxgen -profile NationalschoolOrdererGenesis -channelID ordererchannel -outputBlock nationalschool-genesis.block

# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx ./nationalschool-channel.tx -profile NationalschoolChannel -channelID nationalschoolchannel
